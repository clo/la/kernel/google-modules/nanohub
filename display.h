/*
 * Copyright (C) 2022 Google, Inc.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef _NANOHUB_DISPLAY_H
#define _NANOHUB_DISPLAY_H

ssize_t nanohub_pin_display_select_get(struct device *dev,
                                       struct device_attribute *attr,
                                       char *buf);

ssize_t nanohub_pin_display_select_set(struct device *dev,
                                       struct device_attribute *attr,
                                       const char *buf, size_t count);

#endif

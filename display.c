/*
 * Copyright (C) 2022 Google, Inc.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#include "main.h"
#include "display.h"

ssize_t nanohub_pin_display_select_get(struct device *dev,
                                       struct device_attribute *attr, char *buf)
{
	struct nanohub_data *data = dev_get_drvdata(dev);
	const struct nanohub_platform_data *pdata = data->pdata;
	return scnprintf(buf, PAGE_SIZE, "%d\n",
                         gpio_get_value(pdata->display_select_gpio));
}

ssize_t nanohub_pin_display_select_set(struct device *dev,
                                       struct device_attribute *attr,
                                       const char *buf, size_t count)
{
	struct nanohub_data *data = dev_get_drvdata(dev);
	const struct nanohub_platform_data *pdata = data->pdata;
	if (count >= 1 && buf[0] == '0') {
		gpio_set_value(pdata->display_select_gpio, 0);
	} else if (count >= 1 && buf[0] == '1') {
		gpio_set_value(pdata->display_select_gpio, 1);
	}
	return count;
}
